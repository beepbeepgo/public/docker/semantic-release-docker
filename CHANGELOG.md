## [1.5.2](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/compare/1.5.1...1.5.2) (2023-01-23)


### Bug Fixes

* add aws-cli to image ([ddd484e](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/commit/ddd484e49102a4cd0726de73b4b43a5597309e55))

## [1.5.1](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/compare/1.5.0...1.5.1) (2023-01-08)


### Bug Fixes

* update helm docker version ([183e8e3](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/commit/183e8e3da22c88094b4b43991acf4e25df14614f))

# [1.5.0](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/compare/1.4.0...1.5.0) (2023-01-08)


### Bug Fixes

* download sr package ([d193162](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/commit/d193162b86ce19085b0bfcc6ac23dc8cf921fa16))
* update gitlab ci ([a8d4a27](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/commit/a8d4a2773f32b7ae4730df4bf0d77b70a9d2c9ef))


### Features

* add helm to image ([d1bd60f](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/commit/d1bd60fd723cafe58d10160a45103dc1ce7e878e))

# [1.4.0](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/compare/1.3.8...1.4.0) (2022-12-16)


### Bug Fixes

* **deps:** update dependency @semantic-release/changelog to v6.0.2 ([174a92a](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/commit/174a92abf51bb2d9061e3454e4a224d8b3b86273))
* **deps:** update dependency @semantic-release/gitlab to v9.5.1 ([5daa15b](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/commit/5daa15b44b60b464c18dff7492ca2edd975ff7dd))


### Features

* update project ([cf986f4](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/commit/cf986f4209ad5e1c72a703e1439b3f69c1574501))

# [1.4.0-issue-add-curl-update-smerel.2](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/compare/1.4.0-issue-add-curl-update-smerel.1...1.4.0-issue-add-curl-update-smerel.2) (2022-12-16)


### Bug Fixes

* don't publish as npm package ([502877a](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/commit/502877a05c836b75cd9ba6c098c9b26913bb9e79))

# [1.4.0-issue-add-curl-update-smerel.1](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/compare/1.3.8...1.4.0-issue-add-curl-update-smerel.1) (2022-12-16)


### Bug Fixes

* **deps:** update dependency @semantic-release/changelog to v6.0.2 ([174a92a](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/commit/174a92abf51bb2d9061e3454e4a224d8b3b86273))
* **deps:** update dependency @semantic-release/gitlab to v9.5.1 ([5daa15b](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/commit/5daa15b44b60b464c18dff7492ca2edd975ff7dd))


### Features

* update packages and semantic-release ([ba48992](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/commit/ba48992d897d9ccea4a4917d2d149d8d6ef71386))

# [1.1.0](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/compare/1.0.0...1.1.0) (2022-02-15)


### Features

* NO-TICKET: Staged image with docker and node 14.17 ([a0a87d9](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/commit/a0a87d9ab2bcd638c64a18cb4867f5b1ea05b06f))

# 1.0.0 (2022-02-15)


### Features

* NO-TICKET: Cut Release ([0fb28fd](https://gitlab.com/beepbeepgo/public/docker/semantic-release-docker/commit/0fb28fd9b9442d1219054e3583b5b45974ad35d4))
