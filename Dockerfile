FROM node:18-alpine AS node
FROM docker:20.10.24


COPY --from=python:3.11-alpine . .
COPY --from=node /usr/lib /usr/lib
COPY --from=node /usr/local/share /usr/local/share
COPY --from=node /usr/local/lib /usr/local/lib
COPY --from=node /usr/local/include /usr/local/include
COPY --from=node /usr/local/bin /usr/local/bin
COPY --from=alpine/helm:3.14.4 /usr/bin/helm /usr/bin/helm

RUN apk --update --no-cache upgrade \
    && apk add gcc npm git libffi-dev libc-dev curl aws-cli jq

RUN pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir --upgrade setuptools wheel twine
